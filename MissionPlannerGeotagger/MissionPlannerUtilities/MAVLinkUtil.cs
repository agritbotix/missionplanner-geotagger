﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MissionPlannerUtilities
{
    public static class MAVLinkUtil
    {
        /// <summary>
        /// Convert the log filename to a parsable datetime, since the date created is different than the 
        /// timestamp for the creation of the file
        /// </summary>
        /// <param name="logFileName"></param>
        /// <exception cref="System.FormatException"></exception>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <returns></returns>
        public static DateTime ConvertLogFileNameToValidDateTime(string logFileName)
        {
            logFileName = Path.GetFileName(logFileName);
            logFileName = logFileName.ToLower();
            if (!Path.HasExtension(logFileName))
                logFileName += ".tlog";
            string logfileAsParsableString = null;
            if (Regex.IsMatch(logFileName, @"\D*(\d+?)[_\-\s](\d+?)[_\-\s](\d+?)[_\-\s](\d+?)[_\-\s](\d+?)[_\-\s](\d+)(\D.*?tlog)"))
                logfileAsParsableString = Regex.Replace(logFileName, @"\D*(\d+?)[_\-\s](\d+?)[_\-\s](\d+?)[_\-\s](\d+?)[_\-\s](\d+?)[_\-\s](\d+)(\D.*?tlog)", "$1-$2-$3 $4:$5:$6");
            else
                logfileAsParsableString = Regex.Replace(logFileName, @"(.*\d+?-\d+?-\d+?)[\s_-](\d+?)-(\d+?)-(\d+)(\D.*?tlog)", "$1 $2:$3:$4");
            return DateTime.Parse(logfileAsParsableString);
        }

        /// <summary>
        /// Gets the end of the flight from the flight log.
        /// </summary>
        /// <param name="startOfFlight"></param>
        /// <param name="pathToLogFile"></param>
        /// <returns></returns>
        public static async Task<DateTime> GetEndTimeOfFlight(string pathToLogFile)
        {
            MAVLinkInterface mine = MAVLinkInterface.CreateMavLinkInterfaceFromTelemtryLog(pathToLogFile);

            try
            {
                var startOfFlight = ConvertLogFileNameToValidDateTime(pathToLogFile);

                // Read the first packet so we can get an estimate on the general
                // length of packets.
                mine.logplaybackfile.BaseStream.Position = 0;
                mine.readPacket();
                long i = mine.logplaybackfile.BaseStream.Length - mine.logplaybackfile.BaseStream.Position;

                int runs = 0;

                long maxRuns = 50; // This is two full messages
                DateTime possibleEnd = startOfFlight;
                while (i >= 0 && runs <= maxRuns)
                {
                    await Task.Run(() =>
                    {
                        mine.logplaybackfile.BaseStream.Position = i;
                        mine.readPacket(true);
                        if (mine.lastlogread.Date == startOfFlight.Date &&
                            mine.lastlogread > possibleEnd)
                            possibleEnd = mine.lastlogread;
                    });
                    i -= 1;
                    ++runs;
                }

                return possibleEnd;
            }
            catch(Exception e)
            {
                Console.WriteLine("Could not read from the log file: {0} :: {1}", e.Message, e.StackTrace);
                return DateTime.MaxValue;
            }
            finally
            {
                mine.logplaybackfile.Close();
                mine.logplaybackfile.Dispose();
                mine.Dispose();
            }
        }

        public static List<PointLatLng> ScanPreviousMavLinkRunForPolygonPoints()
        {

            var pointList = new List<PointLatLng>();

            for (int j = 0; j < MAVLinkInterface.wps.Values.Count; ++j)
            {
                var wayPoint = MAVLinkInterface.wps[j];
                if (wayPoint.command == 16) // The type we care about
                {
                    pointList.Add(new PointLatLng(wayPoint.x, wayPoint.y));
                }
            }
            pointList.Sort(new PointLatLongComparer(pointList));
            return pointList;
        }

        public class PointLatLongComparer : Comparer<PointLatLng>
        {
            public PointLatLng Center = PointLatLng.Empty;
            public PointLatLongComparer(List<PointLatLng> pointList)
            {
                double centLat = 0.0;
                double centLng = 0.0;
                foreach (var point in pointList)
                {
                    centLat += point.Lat;
                    centLng += point.Lng;
                }
                Center = new PointLatLng(centLat / (double)pointList.Count, centLng / (double)pointList.Count);
            }

            public override int Compare(PointLatLng A, PointLatLng B)
            {
                //  Variables to Store the atans
                double aTanA, aTanB;

                //  Reference Point
                PointLatLng reference = Center;

                //  Fetch the atans
                aTanA = Math.Atan2(A.Lng - reference.Lng, A.Lat - reference.Lat);
                aTanB = Math.Atan2(B.Lng - reference.Lng, B.Lat - reference.Lat);

                //  Determine next point in Clockwise rotation
                if (aTanA < aTanB) return -1;
                else if (aTanB < aTanA) return 1;
                return 0;
            }
        }

        /// <summary>
        /// Create a new mavlink packet object from a byte array as recieved over mavlink
        /// Endianess will be detetected using packet inspection
        /// </summary>
        /// <typeparam name="TMavlinkPacket">The type of mavlink packet to create</typeparam>
        /// <param name="bytearray">The bytes of the mavlink packet</param>
        /// <param name="startoffset">The position in the byte array where the packet starts</param>
        /// <returns>The newly created mavlink packet</returns>
        public static TMavlinkPacket ByteArrayToStructure<TMavlinkPacket>(this byte[] bytearray, int startoffset = 6) where TMavlinkPacket : struct
        {
            object newPacket = new TMavlinkPacket();
            ByteArrayToStructure(bytearray, ref newPacket, startoffset);
            return (TMavlinkPacket)newPacket;
        }

        public static TMavlinkPacket ByteArrayToStructureBigEndian<TMavlinkPacket>(this byte[] bytearray, int startoffset = 6) where TMavlinkPacket : struct
        {
            object newPacket = new TMavlinkPacket();
            ByteArrayToStructureEndian(bytearray, ref newPacket, startoffset);
            return (TMavlinkPacket)newPacket;
        }

        public static void ByteArrayToStructure(byte[] bytearray, ref object obj, int startoffset)
        {
            if (bytearray[0] == 'U')
            {
                ByteArrayToStructureEndian(bytearray, ref obj, startoffset);
            }
            else
            {

                int len = Marshal.SizeOf(obj);

                IntPtr i = Marshal.AllocHGlobal(len);

                // create structure from ptr
                obj = Marshal.PtrToStructure(i, obj.GetType());

                try
                {
                    // copy byte array to ptr
                    Marshal.Copy(bytearray, startoffset, i, len);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ByteArrayToStructure FAIL " + ex.Message);
                }

                obj = Marshal.PtrToStructure(i, obj.GetType());

                Marshal.FreeHGlobal(i);

            }
        }

        public static void ByteArrayToStructureEndian(byte[] bytearray, ref object obj, int startoffset)
        {

            int len = Marshal.SizeOf(obj);
            IntPtr i = Marshal.AllocHGlobal(len);
            byte[] temparray = (byte[])bytearray.Clone();

            // create structure from ptr
            obj = Marshal.PtrToStructure(i, obj.GetType());

            // do endian swap
            object thisBoxed = obj;
            Type test = thisBoxed.GetType();

            int reversestartoffset = startoffset;

            // Enumerate each structure field using reflection.
            foreach (var field in test.GetFields())
            {
                // field.Name has the field's name.
                object fieldValue = field.GetValue(thisBoxed); // Get value

                // Get the TypeCode enumeration. Multiple types get mapped to a common typecode.
                TypeCode typeCode = Type.GetTypeCode(fieldValue.GetType());

                if (typeCode != TypeCode.Object)
                {
                    Array.Reverse(temparray, reversestartoffset, Marshal.SizeOf(fieldValue));
                    reversestartoffset += Marshal.SizeOf(fieldValue);
                }
                else
                {
                    reversestartoffset += ((byte[])fieldValue).Length;
                }

            }

            try
            {
                // copy byte array to ptr
                Marshal.Copy(temparray, startoffset, i, len);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ByteArrayToStructure FAIL" + ex.ToString());
            }

            obj = Marshal.PtrToStructure(i, obj.GetType());

            Marshal.FreeHGlobal(i);

        }
    }
}

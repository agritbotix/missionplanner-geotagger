﻿using com.drew.imaging.jpg;
using com.drew.lang;
using com.drew.metadata;
using ExifLib;
using log4net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace MissionPlannerUtilities
{
    /// <summary>
    /// You'll want to call this like this:
    /// picturesInfo = doworkGPSOFFSET(logFilePath, dirPictures, seconds);
    /// if (picturesInfo != null)
    /// CreateReportFiles(picturesInfo, dirPictures, seconds);
    /// </summary>
    public class GeoReferencer
    {
        public static readonly string ASSOCIATE_GPS_DATA_WITH_PHOTO_MESSAGE = "Associating GPS data with photo: ";
        public static readonly string TOTAL_READ_BYTES_MESSAGE = "Total Bytes read from Log: ";
        public string UnableToGeotagPhotoPrepend = "skip_me_";

        private string TelemetryLogFilePath;
        /// <summary>
        /// Not always set, currently when set, we assume they want to use that log combined
        /// with the telemetry log.
        /// </summary>
        private string RadioLogFilePath;
        private string OutputDir;
        private bool UseGPSTime = false;
        /// <summary>
        /// An alternate variable to use for the altitude collection, default is to use rel_alt and altASM
        /// </summary>
        public string AlternateAltitudeVar = "";
        private bool OverWriteOriginalImages = false;
        private GeoReferencer()
        {
        }

        public GeoReferencer(Dictionary<long, VehicleLocation> vehicleLocations, string directoryWithImages, string telemetryLogFilePath, bool useGPSTime, bool overWriteOriginalImages, string outputDir = null)
            : this(directoryWithImages, telemetryLogFilePath, useGPSTime, overWriteOriginalImages, outputDir)
        {
            this.VehicleLocations = vehicleLocations;
        }

        /// <summary>
        /// Create a georeferencer for georeferencing image sets
        /// </summary>
        /// <param name="directoryWithImages"></param>
        /// <param name="logFilePath"></param>
        /// <param name="useGPSTime"></param>
        /// <param name="overWriteOriginalImages"></param>
        /// <param name="outputDir"></param>
        public GeoReferencer(string directoryWithImages, string telemetryLogFilePath, bool useGPSTime, bool overWriteOriginalImages, string outputDir = null)
        {
            SourceDirectory = directoryWithImages;
            TelemetryLogFilePath = telemetryLogFilePath;
            UseGPSTime = useGPSTime;
            OverWriteOriginalImages = overWriteOriginalImages;
            if (overWriteOriginalImages)
            {
                outputDir = directoryWithImages;
            }
            else if (outputDir == null)
                outputDir = SourceDirectory + Path.DirectorySeparatorChar + "geotagged";
            OutputDir = outputDir;
        }

        public GeoReferencer(string directoryWithImages, string telemetryLogFilePath, string radioLogFilePath, bool useGPSTime, bool overWriteOriginalImages, string outputDir = null) 
        {
            RadioLogFilePath = radioLogFilePath;
        }

        private const string PHOTO_FILES_FILTER = "*.jpg";
        private const int JXL_ID_OFFSET = 10;

        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private double TaggedPercent = 100.0;

        public class PictureInformation : SingleLocation
        {
            string path;

            public float Rotation
            {
                get;
                set;
            }

            public string Path
            {
                get { return path; }
                set { path = value; }
            }

            DateTime shotTimeReportedByCamera;

            public DateTime ShotTimeReportedByCamera
            {
                get { return shotTimeReportedByCamera; }
                set { shotTimeReportedByCamera = value; }
            }

            int width;
            public int Width
            {
                get { return width; }
                set { width = value; }
            }

            int height;
            public int Height
            {
                get { return height; }
                set { height = value; }
            }

            public PictureInformation()
            {
                width = 3200;
                height = 2400;
            }
        }

        // Key = path of file, Value = object with picture information
        public Dictionary<string, PictureInformation> FilePathPictureInformationMap;

        List<int> JXL_StationIDs = new List<int>();

        // Key = time in milliseconds, Value = object with location info and attitude
        public Dictionary<long, VehicleLocation> VehicleLocations
        {
            get;
            set;
        }

        Hashtable filedatecache = new Hashtable();

        public class SingleLocation
        {

            DateTime time;

            public DateTime Time
            {
                get { return time; }
                set { time = value; }
            }

            double lat;

            public double Lat
            {
                get { return lat; }
                set { lat = value; }
            }
            double lon;

            public double Lon
            {
                get { return lon; }
                set { lon = value; }
            }
            double altAMSL;

            public double AltAMSL
            {
                get { return altAMSL; }
                set { altAMSL = value; }
            }

            double relAlt;

            public double RelAlt
            {
                get { return relAlt; }
                set { relAlt = value; }
            }

            float roll;

            public float Roll
            {
                get { return roll; }
                set { roll = value; }
            }
            float pitch;

            public float Pitch
            {
                get { return pitch; }
                set { pitch = value; }
            }
            float yaw;

            public float Yaw
            {
                get { return yaw; }
                set { yaw = value; }
            }

            public double getAltitude(bool AMSL)
            {
                return (AMSL ? AltAMSL : RelAlt);
            }
        }

        public class VehicleLocation : SingleLocation
        {
            public DateTime gpsTime { get; set; }
            public float GroundCourse { get; set; }
        }

        public class Rational
        {
            uint dem = 0;
            uint num = 0;

            public Rational(double input)
            {
                Value = input;
            }

            public byte[] GetBytes()
            {
                byte[] answer = new byte[8];

                Array.Copy(BitConverter.GetBytes((uint)num), 0, answer, 0, sizeof(uint));
                Array.Copy(BitConverter.GetBytes((uint)dem), 0, answer, 4, sizeof(uint));

                return answer;
            }

            public double Value
            {
                get
                {
                    return num / dem;
                }
                set
                {
                    if ((value % 1.0) != 0)
                    {
                        dem = 100; num = (uint)(value * dem);
                    }
                    else
                    {
                        dem = 1; num = (uint)(value);
                    }
                }
            }
        }

        private string SourceDirectory;

        #region Static Methods

        /// <summary>
        /// This reads from the log file until a gps message is read and gets the offset of the mission planner from
        /// the gps time that was reported.
        /// </summary>
        /// <param name="pathToLogFile"></param>
        /// <returns></returns>
        private static double GetOffsetOfMissionPlannerFromGPS(string pathToLogFile)
        {
            MAVLinkInterface mine = MAVLinkInterface.CreateMavLinkInterfaceFromTelemtryLog(pathToLogFile);
            CurrentState cs = new CurrentState();

            double offset = double.MaxValue;
            while (mine.logplaybackfile.BaseStream.Position < mine.logplaybackfile.BaseStream.Length)
            {
                byte[] packet = mine.readPacket();
                cs.datetime = mine.lastlogread;

                cs.UpdateCurrentSettings(null, true, mine);
                VehicleLocation location = GetLocationFromCurrentState(cs);
                long time = ToMilliseconds(location.Time);
                if (time <= 0 || location.Time.Year != DateTime.Today.Year || location.gpsTime.Year != location.Time.Year)
                    continue;
                offset = location.gpsTime.Subtract(location.Time).TotalSeconds;
                break;
            }
            mine.logplaybackfile.Close();
            mine.logplaybackfile.Dispose();
            mine.Dispose();

            return offset;
        }

        // GPS Log positions
        //Status,Time,NSats,HDop,Lat,Lng,RelAlt,Alt,Spd,GCrs
        //GPS, 3, 122732, 10, 0.00, -35.3628880, 149.1621961, 808.90, 810.30, 23.30, 94.04
        //GPS, 3, 23524837, 1790, 10, 0.00, -35.3629379, 149.165085, 2.09, 585.41, 0.00, 129.86, 0, 4001
        // 0   1     2         3   4    5         6           7        8      9     10     11    12  13
        static int gpsweekpos = 3, timepos = 2, latpos = 6, lngpos = 7, altpos = 8, altAMSLpos = 9;
        // ATT Msg Positions
        // ATT, 199361, 0.00, -0.40, 0.00, -3.01, 103.03, 103.03
        static int pitchATT = 5, rollATT = 3, yawATT = 7;

        private static void ReportProgress(string message)
        {
            Console.WriteLine("Progress: " + message);
        }


        /// <summary>
        /// Runs through the radio log to collect gps messages and returns the dictionary of all the reported locations.
        /// </summary>
        /// <param name="progressReporter"></param>
        /// <param name="fn"></param>
        /// <returns></returns>
        public static async Task<Dictionary<long, VehicleLocation>> ReadRadioLogAsync(string fn)
        {
            return await Task.Run(() =>
            {
                using (StreamReader sr = new StreamReader(fn))
                {
                    Dictionary<long, VehicleLocation> vehiclePositionList = new Dictionary<long, VehicleLocation>();

                    // Will hold the last seen Attitude information in order to incorporate them into the GPS Info
                    float currentYaw = 0f;
                    float currentRoll = 0f;
                    float currentPitch = 0f;

                    VehicleLocation prevLocation = null;
                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();

                        // Look for GPS Messages. However GPS Messages do not have Roll, Pitch and Yaw
                        // So we have to look for one ATT message after having read a GPS one
                        if (line.ToLower().StartsWith("gps"))
                        {
                            VehicleLocation location = new VehicleLocation();

                            string[] gpsLineValues = line.Split(new char[] { ',', ':' });

                            location.Time = GetTimeFromGps(int.Parse(gpsLineValues[gpsweekpos]), int.Parse(gpsLineValues[timepos]));
                            location.Lat = double.Parse(gpsLineValues[latpos]);
                            location.Lon = double.Parse(gpsLineValues[lngpos]);
                            location.RelAlt = double.Parse(gpsLineValues[altpos]);
                            location.AltAMSL = double.Parse(gpsLineValues[altAMSLpos]);

                            location.Roll = currentRoll;
                            location.Pitch = currentPitch;
                            location.Yaw = currentYaw;

                            long millis = ToMilliseconds(location.Time);

                            if (!vehiclePositionList.ContainsKey(millis) && location.Lat != 0.0 && location.Lon != 0.0)
                            {
                                if (millis <= 0 || location.Time.Year != DateTime.Today.Year)
                                    continue;
                                vehiclePositionList.Add(millis, location);
                                // Fill gaps
                                if (prevLocation != null)
                                {
                                    long timeToFillWithPrev = ToMilliseconds(prevLocation.Time) + 1;
                                    while (timeToFillWithPrev < millis)
                                    {
                                        vehiclePositionList[timeToFillWithPrev] = prevLocation;
                                        ++timeToFillWithPrev;
                                    }

                                    if (prevLocation.Time > location.Time)
                                        Console.WriteLine("Location in log file is out of order.");
                                }
                                prevLocation = location;
                            }
                        }
                        else if (line.ToLower().StartsWith("att"))
                        {
                            string[] attLineValues = line.Split(new char[] { ',', ':' });

                            currentRoll = float.Parse(attLineValues[rollATT]);
                            currentPitch = float.Parse(attLineValues[pitchATT]);
                            currentYaw = float.Parse(attLineValues[yawATT]);

                        }


                    }

                    sr.Close();
                    return vehiclePositionList;
                }
            });
        }

        /// <summary>
        /// Get the datetime from a gps radio entry using the week number and milliseconds.
        /// This is needed because of the way the pixhawk records radio entries.
        /// </summary>
        /// <param name="weeknumber">The week number</param>
        /// <param name="milliseconds">The time in milliseconds</param>
        /// <returns></returns>
        public static DateTime GetTimeFromGps(int weeknumber, int milliseconds)
        {
            int LEAP_SECONDS = 25;

            DateTime datum = new DateTime(1980, 1, 6, 0, 0, 0);
            DateTime week = datum.AddDays(weeknumber * 7);
            DateTime time = week.AddMilliseconds(milliseconds);

            time.AddSeconds(-LEAP_SECONDS);

            return time;
        }

        public static async Task<Dictionary<long, VehicleLocation>> ReadGPSMessagesInLogAsync(
            string pathToLogFile, bool useGPSTime, string alternateAltitudeVar = "")
        {
            return await Task.Run(async () =>
                {
                    var flightDate = MissionPlannerUtilities.MAVLinkUtil.ConvertLogFileNameToValidDateTime(Path.GetFileName(pathToLogFile));
                    Dictionary<long, VehicleLocation> vehiclePositionList = new Dictionary<long, VehicleLocation>();

                    // Telemetry Log
                    if (pathToLogFile.ToLower().EndsWith("tlog"))
                    {
                        MAVLinkInterface mine = MAVLinkInterface.CreateMavLinkInterfaceFromTelemtryLog(pathToLogFile);

                        CurrentState cs = new CurrentState();
                        VehicleLocation prevLocation = null;

                        ReportProgress(TOTAL_READ_BYTES_MESSAGE +  "0/" + mine.logplaybackfile.BaseStream.Length);
                        int i = 0;
                        while (mine.logplaybackfile.BaseStream.Position < mine.logplaybackfile.BaseStream.Length)
                        {
                            await Task.Run(() =>
                            {
                                byte[] packet = mine.readPacket();
                                i++;
                                cs.datetime = mine.lastlogread;

                                cs.UpdateCurrentSettings(null, true, mine);
                                if (i % 1024 == 0)
                                {
                                    log.Info(cs.datetime + " mills " + ToMilliseconds(cs.datetime));
                                }

                                VehicleLocation location = GetLocationFromCurrentState(cs, alternateAltitudeVar);

                                if (useGPSTime && cs.gpstime != DateTime.MinValue && cs.gpstime != DateTime.MaxValue)
                                {
                                    location.Time = cs.gpstime.ToLocalTime();
                                }

                                if (i % 1024 == 0)
                                {
                                    ReportProgress(TOTAL_READ_BYTES_MESSAGE + mine.logplaybackfile.BaseStream.Position + "/" + mine.logplaybackfile.BaseStream.Length);
                                }

                                long time = FlattenTime(ToMilliseconds(location.Time));
                                if (!vehiclePositionList.ContainsKey(time) && location.Lat != 0.0 && location.Lon != 0.0)
                                {
                                    if (time <= 0 || location.Time.Year != flightDate.Year)
                                        return;
                                    vehiclePositionList.Add(time, location);
                                    // Fill gaps
                                    if (prevLocation != null)
                                    {
                                        long timeToFillWithPrev = FlattenTime(ToMilliseconds(location.Time)) + 100;
                                        while (timeToFillWithPrev < time)
                                        {
                                            vehiclePositionList[timeToFillWithPrev] = prevLocation;
                                            timeToFillWithPrev += 100;
                                        }

                                        if (prevLocation.Time > location.Time)
                                            Console.WriteLine("Location in log file is out of order.");
                                    }
                                    prevLocation = location;
                                }
                            });
                        }
                        mine.logplaybackfile.Close();
                        mine.logplaybackfile.Dispose();
                        mine.Dispose();
                    }

                    return vehiclePositionList;
                });
        }

        /// <summary>
        /// Creates a new vehicle location from the current state.
        /// </summary>
        /// <param name="cs"></param>
        /// <returns></returns>
        private static VehicleLocation GetLocationFromCurrentState(CurrentState cs, string alternateAltitudeVariable = "")
        {
            VehicleLocation location = new VehicleLocation();
            location.Time = cs.datetime;
            location.gpsTime = cs.gpstime;
            location.Lat = cs.lat;
            location.Lon = cs.lng;
            location.RelAlt = cs.alt;
            location.AltAMSL = cs.altasl;
            if (!String.IsNullOrWhiteSpace(alternateAltitudeVariable))
            {
                location.RelAlt = (float)cs.getValue(alternateAltitudeVariable);
                location.AltAMSL = (float)cs.getValue(alternateAltitudeVariable);
            }

            location.Roll = cs.roll;
            location.Pitch = cs.pitch;
            location.Yaw = cs.yaw;
            location.GroundCourse = cs.groundcourse;
            return location;
        }

        /// <summary>
        /// Converts to unix time... not sure why, this was inherited code
        /// TODO: Find out why this is.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static long ToMilliseconds(DateTime date)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return Convert.ToInt64((date - epoch).TotalMilliseconds);
        }

        #endregion

        #region Private Methods
        private void WriteCoordinatesToImage(string sourceFilePath, bool overWrite, double dLat, double dLong, double alt, float rotation)
        {
            string outputfilename = sourceFilePath;

            // Save file into Geotag folder
            string geoTagFolder = OutputDir;

            if (!overWrite)
            {
                outputfilename = geoTagFolder + Path.DirectorySeparatorChar + Path.GetFileNameWithoutExtension(sourceFilePath) + "_geotag" + Path.GetExtension(sourceFilePath);

                // Just in case
                if (File.Exists(outputfilename))
                    File.Delete(outputfilename);
            }

            using (MemoryStream ms = new MemoryStream(File.ReadAllBytes(sourceFilePath)))
            {
                log.Info("GeoTagging " + sourceFilePath);

                using (Image Pic = Image.FromStream(ms))
                {
                    PropertyItem[] pi = Pic.PropertyItems;

                    pi[0].Id = 0x0004;
                    pi[0].Type = 5;
                    pi[0].Len = sizeof(ulong) * 3;
                    pi[0].Value = CoordToByteArray(dLong);
                    Pic.SetPropertyItem(pi[0]);

                    pi[0].Id = 0x0002;
                    pi[0].Type = 5;
                    pi[0].Len = sizeof(ulong) * 3;
                    pi[0].Value = CoordToByteArray(dLat);
                    Pic.SetPropertyItem(pi[0]);

                    if (alt >= 0)
                    {
                        pi[0].Id = 0x0006;
                        pi[0].Type = 5;
                        pi[0].Len = 8;
                        pi[0].Value = new Rational(alt).GetBytes();
                        Pic.SetPropertyItem(pi[0]);
                    }

                    pi[0].Id = 1;
                    pi[0].Len = 2;
                    pi[0].Type = 2;

                    if (dLat < 0)
                    {
                        pi[0].Value = new byte[] { (byte)'S', 0 };
                    }
                    else
                    {
                        pi[0].Value = new byte[] { (byte)'N', 0 };
                    }
                    Pic.SetPropertyItem(pi[0]);

                    pi[0].Id = 3;
                    pi[0].Len = 2;
                    pi[0].Type = 2;
                    if (dLong < 0)
                    {
                        pi[0].Value = new byte[] { (byte)'W', 0 };
                    }
                    else
                    {
                        pi[0].Value = new byte[] { (byte)'E', 0 };
                    }
                    Pic.SetPropertyItem(pi[0]);

                    Pic.Save(outputfilename);
                }
            }
        }

        private VehicleLocation LookForLocation(DateTime t, Dictionary<long, VehicleLocation> listLocations)
        {
            Console.WriteLine(t.ToString());

            long time = ToMilliseconds(t);
            Console.WriteLine("Milliseconds: " + time);

            // Flatten time to 100 millis
            long flattenTime = FlattenTime(time);
            int millisSTEP = 100;

            // 2 seconds in the log as absolute maximum
            int maxIteration = 20;

            bool found = false;
            int iteration = 0;
            VehicleLocation location = null;

            while (!found && iteration < maxIteration)
            {
                found = listLocations.ContainsKey(flattenTime);
                if (found)
                {
                    location = listLocations[flattenTime];
                }
                else
                {
                    flattenTime += millisSTEP;
                    iteration++;
                }
            }

            return location;
        }

        /// <summary>
        /// Since the accuracy of the camera is only at max 100 milliseconds, this will flatten to the nearest 100 milliseconds
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        private static long FlattenTime(long time)
        {
            long flattenTime = (time / 100) * 100;
            return flattenTime;
        }

        private DateTime getPhotoTime(string fn)
        {
            DateTime dtaken = DateTime.MinValue;

            if (filedatecache.ContainsKey(fn))
            {
                return (DateTime)filedatecache[fn];
            }

            try
            {

                Metadata lcMetadata = null;
                try
                {
                    FileInfo lcImgFile = new FileInfo(fn);
                    // Loading all meta data
                    lcMetadata = JpegMetadataReader.ReadMetadata(lcImgFile);
                }
                catch (JpegProcessingException e)
                {
                    log.InfoFormat(e.Message);
                    return dtaken;
                }

                foreach (AbstractDirectory lcDirectory in lcMetadata)
                {

                    if (lcDirectory.ContainsTag(0x9003))
                    {
                        dtaken = lcDirectory.GetDate(0x9003);
                        log.InfoFormat("does " + lcDirectory.GetTagName(0x9003) + " " + dtaken);

                        filedatecache[fn] = dtaken;

                        break;
                    }

                }





                ////// old method, works, just slow
                /*
                Image myImage = Image.FromFile(fn);
                PropertyItem propItem = myImage.GetPropertyItem(36867); // 36867  // 306

                //Convert date taken metadata to a DateTime object 
                string sdate = Encoding.UTF8.GetString(propItem.Value).Trim();
                string secondhalf = sdate.Substring(sdate.IndexOf(" "), (sdate.Length - sdate.IndexOf(" ")));
                string firsthalf = sdate.Substring(0, 10);
                firsthalf = firsthalf.Replace(":", "-");
                sdate = firsthalf + secondhalf;
                dtaken = DateTime.Parse(sdate);

                myImage.Dispose();
                 */
            }
            catch { }

            return dtaken;
        }

        private void MarkFileForSkippingDuringProcessing(string pathToFile)
        {
            var originalFile = new FileInfo(pathToFile);
            originalFile.MoveTo(Path.Combine(originalFile.DirectoryName, UnableToGeotagPhotoPrepend + originalFile.Name));
        }

        /// <summary>
        /// Returns the list of images and their locations
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="deletePhotosBelowAltitude"></param>
        /// <param name="textFileForPicInfo"></param>
        /// <returns></returns>
        private async Task<string[]> ExtractDataFromLogAndGeoTagPhotosAsync(float offset, bool deletePhotosBelowAltitude, string textFileForPicInfo)
        {
            await AttemptToMapPhotosToTelemetryLogAsync(offset);
            var retList = new List<string>();
            const char seperator = '|';

            if (VehicleLocations == null || FilePathPictureInformationMap == null)
            {
                log.Error(String.Format("No vehicle locations: {0}, No Images: {1}", VehicleLocations == null, FilePathPictureInformationMap == null));
                return null;
            }

            int i = 0;
            ReportProgress("Pictures Geotagged: 0/" + FilePathPictureInformationMap.Values.Count);

            double flightAverageAltitude = 0;
            if (deletePhotosBelowAltitude)
                foreach (PictureInformation picInfo in FilePathPictureInformationMap.Values)
                    flightAverageAltitude += picInfo.RelAlt;
            flightAverageAltitude /= (double)FilePathPictureInformationMap.Count;
            foreach (PictureInformation picInfo in FilePathPictureInformationMap.Values)
            {
                await Task.Run(() =>
                {
                    if (deletePhotosBelowAltitude && flightAverageAltitude - picInfo.RelAlt >= 30.0)
                    {
                        log.Info("Deleting " + picInfo.Path + " for altitude " + picInfo.RelAlt);
                        MarkFileForSkippingDuringProcessing(picInfo.Path);
                    }
                    else
                    {
                        retList.Add(
                            // Due to some cameras having the same file names but
                            // different directories, append.
                            Path.Combine(Path.GetDirectoryName(picInfo.Path), Path.GetFileName(picInfo.Path)) 
                            + seperator + picInfo.Lon + seperator + picInfo.Lat + seperator + picInfo.AltAMSL + seperator + picInfo.Yaw + seperator + picInfo.Pitch + seperator + picInfo.Roll);
                        if (textFileForPicInfo == null)
                        {
                            try
                            {
                                WriteCoordinatesToImage(picInfo.Path, OverWriteOriginalImages, picInfo.Lat, picInfo.Lon, picInfo.AltAMSL, picInfo.Rotation);
                            }
                            catch(Exception e)
                            {
                                MarkFileForSkippingDuringProcessing(picInfo.Path);
                                log.Error(String.Format("Couldn't write image coordinates: {0}", picInfo.Path), e);
                            }
                        }
                    }
                });
                ReportProgress("Pictures Geotagged: " + (++i) + "/" + FilePathPictureInformationMap.Values.Count);
            }

            retList.Insert(0, "Name|Longitude|Latitude|Altitude|Yaw|Pitch|Roll");
            if (textFileForPicInfo != null)
            {
                System.IO.File.WriteAllLines(textFileForPicInfo, retList.ToArray());
            }
            
            Console.WriteLine("GeoTagging FINISHED with " + TaggedPercent + "% photos tagged \n\n");

            return retList.ToArray();
        }

        /// <summary>
        /// Scans the photos and utilizes the offset to see if we can map the images to the telemetry log. Will
        /// set the FilePathPictureInformationMap that could be used in the next step.
        /// </summary>
        /// <param name="offset">The offset of the camera</param>
        /// <returns></returns>
        private async Task AttemptToMapPhotosToTelemetryLogAsync(float offset)
        {
            ReportProgress("Starting Geotagging of Photos");

            bool isExists = System.IO.Directory.Exists(OutputDir);

            if (!OverWriteOriginalImages)
            {
                // delete old files and folder
                if (isExists)
                    Directory.Delete(OutputDir, true);

                // create it again
                System.IO.Directory.CreateDirectory(OutputDir);
            }

            FilePathPictureInformationMap = await GetInformationFromLogFileAsync(offset);
        }

        private enum LoopAction
        {
            Break, Continue, Return
        }

        /// <summary>
        /// Scans the SourceDirectory for jpegs and then attempt to match them with their picture information. Upon completion
        /// will have set the TaggedPercent variable to a value between 0 and 100.
        /// </summary>
        /// <param name="offsetInSeconds">The offest to use, negative means the camera is BEHIND missionplanner</param>
        /// <returns></returns>
        private async Task<Dictionary<string, PictureInformation>> GetInformationFromLogFileAsync(float offsetInSeconds)
        {
            // Lets start over 
            Dictionary<string, PictureInformation> picturesInformationTemp = new Dictionary<string, PictureInformation>();

            //logFile = @"C:\Users\hog\Pictures\farm 1-10-2011\100SSCAM\2011-10-01 11-48 1.log";
            log.Info("Reading log for GPS-ATT Messages");

            // Read Vehicle Locations from log. GPS Messages. Will have to do it anyway
            if (VehicleLocations == null || VehicleLocations.Count <= 0)
            {
                if (!String.IsNullOrEmpty(RadioLogFilePath))
                {
                    var offset = GetOffsetOfMissionPlannerFromGPS(TelemetryLogFilePath);
                    offsetInSeconds -= (float)Math.Round(offset);
                    VehicleLocations = await ReadRadioLogAsync(RadioLogFilePath);
                }
                else
                {
                    VehicleLocations = await ReadGPSMessagesInLogAsync(TelemetryLogFilePath, UseGPSTime, AlternateAltitudeVar);
                }
            }

            if (VehicleLocations == null)
            {
                log.Info("Log file problem. Aborting....\n");
                return null;
            }

            //dirWithImages = @"C:\Users\hog\Pictures\farm 1-10-2011\100SSCAM";

            log.Info("Read images from " + SourceDirectory);

            string[] files = Directory.GetFiles(SourceDirectory, "*.jpg");

            log.Info("Images read : " + files.Length + "\n");

            // Check that we have at least one picture
            if (files.Length <= 0)
            {
                log.Info("Not enough files found.  Aborting..... \n");
                return null;
            }

            Array.Sort(files, Comparer.DefaultInvariant);

            ReportProgress(ASSOCIATE_GPS_DATA_WITH_PHOTO_MESSAGE + "0/" + files.Length);

            var startMilliseconds = VehicleLocations.First().Key;
            foreach (var key in VehicleLocations.Keys)
                startMilliseconds = Math.Min(key, startMilliseconds);


            VehicleLocation lastLocation = null;
            int badPhotos = 0;

            // Each file corresponds to one CAM message
            // We assume that picture names are in ascending order in time
            for (int i = 0; i < files.Length; i++)
            {
                var loopAction = await Task.Run<LoopAction>(() =>
                {
                    ReportProgress(ASSOCIATE_GPS_DATA_WITH_PHOTO_MESSAGE +  (i + 1) + "/" + files.Length);

                    string filename = files[i];

                    PictureInformation p = new PictureInformation();

                    // Fill shot time in Picture
                    p.ShotTimeReportedByCamera = getPhotoTime(filename);

                    if (p.ShotTimeReportedByCamera == DateTime.MinValue ||
                        p.ShotTimeReportedByCamera == DateTime.MaxValue)
                    {
                        log.Error(String.Format("Date time wrong for image: {0}, moving on", filename));
                        MarkFileForSkippingDuringProcessing(filename);
                        badPhotos++;
                        return LoopAction.Continue;
                    }

                    // Look for corresponding Location in vehicleLocationList
                    var offsetTime = p.ShotTimeReportedByCamera.AddSeconds(-offsetInSeconds);
                    VehicleLocation shotLocation = LookForLocation(offsetTime, VehicleLocations);

                    // Check to see if the current image is just behind the first recorded location of the flight
                    // Until we get to a picture to put in, this may result in no pictures able to be geotagged
                    if (shotLocation == null && picturesInformationTemp.Count <= 0)
                    {
                        var milliseconds = ToMilliseconds(offsetTime);
                        var difference = Math.Abs(milliseconds - startMilliseconds);
                        if (difference < TimeSpan.FromHours(1).TotalMilliseconds)
                        {
                            log.Info(String.Format("{0} was {1} milliseconds different from the start of the flight moving to next image (less than 1 hour).", filename, difference));
                            return LoopAction.Continue;
                        }
                        // Now check to see if we need to find an hour offset because the image wasn't found
                        log.Debug("Checking for hour offset");
                        for (int hourOffset = -12; i == 0 && hourOffset <= 12 && shotLocation == null; ++hourOffset)
                        {
                            float tempOffset = offsetInSeconds + (float)(hourOffset * 60 * 60);
                            shotLocation = LookForLocation(p.ShotTimeReportedByCamera.AddSeconds(-tempOffset), VehicleLocations);
                            if (shotLocation != null)
                            {
                                log.Info("Note that the offset was changed by " + hourOffset + " hour(s).");
                                offsetInSeconds = tempOffset;
                                break;
                            }
                        }
                    }

                    if (shotLocation == null)
                    {
                        TaggedPercent = 100.0 * (double)picturesInformationTemp.Count / (double)files.Length;
                        if (lastLocation != null)
                        {
                            shotLocation = lastLocation;
                            ++badPhotos;
                        }
                        else
                        {
                            //if (MyProgressReporter == null)
                            //{
                            //    picturesInformationTemp = null;
                            //    log.Info("Couldn't find image in flight log. Exiting.");
                            //}
                            return LoopAction.Return;
                        }
                    }

                    lastLocation = shotLocation;

                    p.Lat = shotLocation.Lat;
                    p.Lon = shotLocation.Lon;
                    p.AltAMSL = shotLocation.AltAMSL;

                    p.RelAlt = shotLocation.RelAlt;

                    p.Pitch = shotLocation.Pitch;
                    p.Roll = shotLocation.Roll;
                    p.Yaw = shotLocation.Yaw;
                    p.Rotation = 360 - shotLocation.GroundCourse;

                    p.Time = shotLocation.Time.ToLocalTime();

                    p.Path = filename;


                    picturesInformationTemp.Add(filename, p);

                    log.Info("Photo " + filename + " processed\n");
                    return LoopAction.Continue;
                });
                if (loopAction == LoopAction.Break)
                    break;
                if (loopAction == LoopAction.Return)
                    return picturesInformationTemp;
            }
            TaggedPercent = 100.0;
            if (badPhotos > 0)
                log.Error(String.Format("WARNING: {0} photos were associated with the location of the previous photo due to bad GPS signal", badPhotos));
            return picturesInformationTemp;
        }

        /// <summary>
        /// Converts a double to a byte array to be used in the writing to the image
        /// </summary>
        /// <param name="coordin"></param>
        /// <returns></returns>
        private byte[] CoordToByteArray(double coordin)
        {
            double coord = Math.Abs(coordin);

            byte[] output = new byte[sizeof(double) * 3];

            int d = (int)coord;
            int m = (int)((coord - d) * 60);
            double s = ((((coord - d) * 60) - m) * 60);
            /*
21 00 00 00 01 00 00 00--> 33/1
18 00 00 00 01 00 00 00--> 24/1
06 02 00 00 0A 00 00 00--> 518/10
*/

            Array.Copy(BitConverter.GetBytes((uint)d), 0, output, 0, sizeof(uint));
            Array.Copy(BitConverter.GetBytes((uint)1), 0, output, 4, sizeof(uint));
            Array.Copy(BitConverter.GetBytes((uint)m), 0, output, 8, sizeof(uint));
            Array.Copy(BitConverter.GetBytes((uint)1), 0, output, 12, sizeof(uint));
            Array.Copy(BitConverter.GetBytes((uint)(s * 10)), 0, output, 16, sizeof(uint));
            Array.Copy(BitConverter.GetBytes((uint)10), 0, output, 20, sizeof(uint));

            return output;
        }

        #endregion

        /// <summary>
        /// Combines the log data from the Mission Planner flight to put GPS information into
        /// the exif of the images.
        /// </summary>
        /// <param name="offset">How many seconds the camera is BEHIND the flight controller.  Use a POSITIVE
        /// number if the camera is AHEAD of the flight controller</param>
        public void GeoTagPhotos(float offset, bool deletePhotosBelowAltitude = false)
        {
            ExtractDataFromLogAndGeoTagPhotosAsync(offset, deletePhotosBelowAltitude, null).Wait();
        }

        public async Task<string[]> GeoTagPhotosAsync(float offset, bool deletePhotosBelowAltitude = false)
        {
            return await ExtractDataFromLogAndGeoTagPhotosAsync(offset, deletePhotosBelowAltitude, null);
        }

        public async Task GeoTagPhotosWithTextFileForData(float offset, bool deletePhotosBelowAltitude, string textFileForData)
        {
            await ExtractDataFromLogAndGeoTagPhotosAsync(offset, deletePhotosBelowAltitude, textFileForData);
        }

        /// <summary>
        /// Returns an array of strings that represent the geotagging data for loading into a processing utility.
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="deletePhotosBelowAltitude"></param>
        /// <returns></returns>
        public string[] GenerateGeotagTextFile(float offset, bool deletePhotosBelowAltitude = false)
        {
            string outputFile = Path.Combine(OutputDir, "picInfo.txt");
            var task = ExtractDataFromLogAndGeoTagPhotosAsync(offset, deletePhotosBelowAltitude, outputFile);
            task.Wait();
            return task.Result;
        }
    }
}

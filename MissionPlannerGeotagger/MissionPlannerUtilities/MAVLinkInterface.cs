﻿using log4net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Subjects;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MissionPlannerUtilities
{
    public class MAVLinkInterface : IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public MAVState MAV = new MAVState();

        /// <summary>
        /// Creates a new mavlink interface, properly initialized with a logplaybackfile for reading through.
        /// The log playbackfile should be closed and disposed when finished.
        /// </summary>
        /// <param name="pathToLogFile"></param>
        /// <returns></returns>
        public static MAVLinkInterface CreateMavLinkInterfaceFromTelemtryLog(string pathToLogFile)
        {
            MAVLinkInterface mine = new MAVLinkInterface(Path.GetFileNameWithoutExtension(pathToLogFile));
            mine.logplaybackfile = new BinaryReader(File.Open(pathToLogFile, FileMode.Open, FileAccess.Read, FileShare.Read));
            mine.logreadmode = true;

            mine.MAV.packets.Initialize(); // clear
            return mine;
        }

        public MAVLinkInterface(string logFileName = null)
        {
            // init fields
            //this.BaseStream = new SerialPort();
            //this.packetcount = 0;
            this.packetspersecond = new double[0x100];
            this.packetspersecondbuild = new DateTime[0x100];
            this._bytesReceivedSubj = new Subject<int>();
            //this._bytesSentSubj = new Subject<int>();
            this.WhenPacketLost = new Subject<int>();
            this.WhenPacketReceived = new Subject<int>();
            this.readlock = new object();
            this.lastvalidpacket = DateTime.MinValue;
            this.oldlogformat = false;
            //this.mavlinkversion = 0;

            //this.debugmavlink = false;
            this.logreadmode = false;
            this.lastlogread = DateTime.MinValue;

            if (logFileName != null)
                expectedStartTime = MAVLinkUtil.ConvertLogFileNameToValidDateTime(logFileName);
            this.logplaybackfile = null;
            //this.logfile = null;
            this.rawlogfile = null;
            this.bps1 = 0;
            this.bps2 = 0;
            this.bps = 0;
            this.bpstime = DateTime.MinValue;

            this.packetslost = 0f;
            this.packetsnotlost = 0f;
            this.packetlosttimer = DateTime.MinValue;
            this.lastbad = new byte[2];
        }

        /// <summary>
        /// time seen of last mavlink packet
        /// </summary>
        public DateTime lastvalidpacket { get; set; }

        public class MAVState
        {
            public MAVState()
            {
                this.sysid = 0;
                this.compid = 0;
                this.param = new Hashtable();
                this.packets = new byte[0x100][];
                this.packetseencount = new int[0x100];
                this.aptype = 0;
                this.apname = 0;
                this.recvpacketcount = 0;
                this.VersionString = "";
                this.SoftwareVersions = "";
                this.SerialString = "";
            }

            // all
            public string VersionString { get; set; }
            // px4+ only
            public string SoftwareVersions { get; set; }
            // px4+ only
            public string SerialString { get; set; }
            /// <summary>
            /// the static global state of the currently connected MAV
            /// </summary>
            public CurrentState cs = new CurrentState();
            /// <summary>
            /// mavlink remote sysid
            /// </summary>
            public byte sysid { get; set; }
            /// <summary>
            /// mavlink remove compid
            /// </summary>
            public byte compid { get; set; }
            /// <summary>
            /// storage for whole paramater list
            /// </summary>
            public Hashtable param { get; set; }
            /// <summary>
            /// storage of a previous packet recevied of a specific type
            /// </summary>
            public byte[][] packets { get; set; }
            public int[] packetseencount { get; set; }
            /// <summary>
            /// mavlink ap type
            /// </summary>
            public MAVLink.MAV_TYPE aptype { get; set; }
            public MAVLink.MAV_AUTOPILOT apname { get; set; }
            public Common.ap_product Product_ID { get { if (param.ContainsKey("INS_PRODUCT_ID")) return (Common.ap_product)(float)param["INS_PRODUCT_ID"]; return Common.ap_product.AP_PRODUCT_ID_NONE; } }
            /// <summary>
            /// used as a snapshot of what is loaded on the ap atm. - derived from the stream
            /// </summary>
            public Dictionary<int, MAVLink.mavlink_mission_item_t> wps = new Dictionary<int, MAVLink.mavlink_mission_item_t>();

            public Dictionary<int, MAVLink.mavlink_rally_point_t> rallypoints = new Dictionary<int, MAVLink.mavlink_rally_point_t>();

            public Dictionary<int, MAVLink.mavlink_fence_point_t> fencepoints = new Dictionary<int, MAVLink.mavlink_fence_point_t>();

            /// <summary>
            /// Store the guided mode wp location
            /// </summary>
            public MAVLink.mavlink_mission_item_t GuidedMode = new MAVLink.mavlink_mission_item_t();

            internal int recvpacketcount = 0;
        }

        /// <summary>
        /// used to calc packets per second on any single message type - used for stream rate comparaison
        /// </summary>
        public double[] packetspersecond { get; set; }
        /// <summary>
        /// time last seen a packet of a type
        /// </summary>
        DateTime[] packetspersecondbuild = new DateTime[256];

        /// <summary>
        /// Observable of the count of packets skipped (on reception), 
        /// calculated from periods where received packet sequence is not
        /// contiguous
        /// </summary>
        public Subject<int> WhenPacketLost { get; set; }

        public Subject<int> WhenPacketReceived { get; set; }


        /// <summary>
        /// used as a serial port write lock
        /// </summary>
        volatile object objlock = new object();

        public bool logreadmode = true;
        private volatile object readlock = new object();
        //private static ICommsSerial BaseStream;
        public byte[] lastbad;
        public BinaryReader logplaybackfile;
        public bool oldlogformat = false;
        public BufferedStream rawlogfile = null;
        public string buildplaintxtline = "";
        public string plaintxtline = "";
        public readonly Subject<int> _bytesReceivedSubj = new Subject<int>();
        public DateTime packetlosttimer;
        public float packetslost = 0;
        public float packetsnotlost = 0;
        private DateTime _lastlogread;
        /// <summary>
        /// This will be in local time
        /// </summary>
        public  DateTime lastlogread 
        {
            get
            {
                return _lastlogread;
            }
            set
            {
                if (expectedStartTime != DateTime.MinValue 
                    && value.Year == expectedStartTime.Year
                    && !hourOffsetSet)
                {
                    // + 17 hours
                    var seconds = (double)expectedStartTime.Subtract(value).TotalSeconds;
                    hourOffset = (int)(Math.Round(seconds / 3600.0));
                    hourOffsetSet = true;
                }
                _lastlogread = value.AddHours(hourOffset);
            }
        }
        private bool hourOffsetSet = false;
        public int hourOffset = 0;
        public DateTime expectedStartTime = DateTime.MinValue;
        public int bps1 = 0;
        public int bps2 = 0;
        public  DateTime bpstime { get; set; }
        public  int bps { get; set; }
        public  float synclost;

        public const byte MAVLINK_VERSION = 2;


        /// <summary>
        /// Serial Reader to read mavlink packets. POLL method
        /// </summary>
        /// <returns></returns>
        public byte[] readPacket(bool smartSearch = false)
        {
            byte[] buffer = new byte[260];
            int count = 0;
            int length = 0;
            int readcount = 0;
            lastbad = new byte[2];

            //BaseStream.ReadTimeout = 1200; // 1200 ms between chars - the gps detection requires this.

            DateTime start = DateTime.Now;

            //Console.WriteLine(DateTime.Now.Millisecond + " SR0 " + BaseStream.BytesToRead);

            lock (readlock)
            {
                //Console.WriteLine(DateTime.Now.Millisecond + " SR1 " + BaseStream.BytesToRead);

                while (logreadmode)
                {
                    try
                    {
                        if (readcount > 300)
                        {
                            log.Info("MAVLink readpacket No valid mavlink packets");
                            break;
                        }
                        readcount++;
                        if (logreadmode)
                        {
                            oldlogformat = false;

                            if (oldlogformat)
                            {
                                buffer = readlogPacket(); //old style log
                            }
                            else
                            {
                                buffer = readlogPacketMavlink(smartSearch);
                                if (buffer == null && smartSearch)
                                    return null;
                            }
                        }
                        else
                        {
                            //MAV.cs.datetime = DateTime.Now;

                            //Console.WriteLine(DateTime.Now.Millisecond + " SR1a " + BaseStream.BytesToRead);
                            //Console.WriteLine(DateTime.Now.Millisecond + " SR1b " + BaseStream.BytesToRead);
                        }
                    }
                    catch (Exception e) { log.Info("MAVLink readpacket read error: " + e.ToString()); break; }

                    // check if looks like a mavlink packet and check for exclusions and write to console
                    if (buffer[0] != 254)
                    {
                        if (buffer[0] >= 0x20 && buffer[0] <= 127 || buffer[0] == '\n' || buffer[0] == '\r')
                        {
                            // check for line termination
                            if (buffer[0] == '\r' || buffer[0] == '\n')
                            {
                                // check new line is valid
                                if (buildplaintxtline.Length > 3)
                                    plaintxtline = buildplaintxtline;

                                // reset for next line
                                buildplaintxtline = "";
                            }

                            //TCPConsole.Write(buffer[0]);
                            log.Debug((char)buffer[0]);
                            buildplaintxtline += (char)buffer[0];
                        }
                        _bytesReceivedSubj.OnNext(1);
                        count = 0;
                        lastbad[0] = lastbad[1];
                        lastbad[1] = buffer[0];
                        buffer[1] = 0;
                        continue;
                    }
                    // reset count on valid packet
                    readcount = 0;

                    //Console.WriteLine(DateTime.Now.Millisecond + " SR2 " + BaseStream.BytesToRead);

                    // check for a header
                    if (buffer[0] == 254)
                    {

                        // packet length
                        length = buffer[1] + 6 + 2 - 2; // data + header + checksum - U - length
                        if (count >= 5 || logreadmode)
                        {
                            //if (MAV.sysid != 0)
                            //{
                            //    if (MAV.sysid != buffer[3] || MAV.compid != buffer[4])
                            //    {
                            //        if (buffer[3] == '3' && buffer[4] == 'D')
                            //        {
                            //            // this is a 3dr radio rssi packet
                            //        }
                            //        else
                            //        {
                            //            Console.WriteLine("Mavlink Bad Packet (not addressed to this MAV) got {0} {1} vs {2} {3}", buffer[3], buffer[4], MAV.sysid, MAV.compid);
                            //            return new byte[0];
                            //        }
                            //    }
                            //}

                            try
                            {
                                if (logreadmode)
                                {

                                }
                                else
                                {
                                    //basestream crap
                                }
                                count = length + 2;
                            }
                            catch { break; }
                            break;
                        }
                    }

                    count++;
                    if (count == 299)
                        break;
                }

                //Console.WriteLine(DateTime.Now.Millisecond + " SR3 " + BaseStream.BytesToRead);
            }// end readlock

            Array.Resize<byte>(ref buffer, count);

            _bytesReceivedSubj.OnNext(buffer.Length);

            if (!logreadmode && packetlosttimer.AddSeconds(5) < DateTime.Now)
            {
                packetlosttimer = DateTime.Now;
                packetslost = (packetslost * 0.8f);
                packetsnotlost = (packetsnotlost * 0.8f);
            }
            else if (logreadmode && packetlosttimer.AddSeconds(5) < lastlogread)
            {
                packetlosttimer = lastlogread;
                packetslost = (packetslost * 0.8f);
                packetsnotlost = (packetsnotlost * 0.8f);
            }

            //MAV.cs.linkqualitygcs = (ushort)((packetsnotlost / (packetsnotlost + packetslost)) * 100.0);

            //if (bpstime.Second != DateTime.Now.Second && !logreadmode && BaseStream.IsOpen)
            //{
            //    Console.Write("bps {0} loss {1} left {2} mem {3}      \n", bps1, synclost, BaseStream.BytesToRead, System.GC.GetTotalMemory(false) / 1024 / 1024.0);
            //    bps2 = bps1; // prev sec
            //    bps1 = 0; // current sec
            //    bpstime = DateTime.Now;
            //}

            bps1 += buffer.Length;

            bps = (bps1 + bps2) / 2;

            if (buffer.Length >= 5 && (buffer[3] == 255 || buffer[3] == 253) && logreadmode) // gcs packet
            {
                getWPsfromstream(ref buffer);
                return buffer;// new byte[0];
            }

            ushort crc = MavlinkCRC.crc_calculate(buffer, buffer.Length - 2);

            if (buffer.Length > 5 && buffer[0] == 254)
            {
                crc = MavlinkCRC.crc_accumulate(MAVLink.MAVLINK_MESSAGE_CRCS[buffer[5]], crc);
            }

            if (buffer.Length > 5 && buffer[1] != MAVLink.MAVLINK_MESSAGE_LENGTHS[buffer[5]])
            {
                if (MAVLink.MAVLINK_MESSAGE_LENGTHS[buffer[5]] == 0) // pass for unknown packets
                {

                }
                else
                {
                    log.InfoFormat("Mavlink Bad Packet (Len Fail) len {0} pkno {1}", buffer.Length, buffer[5]);
                    if (buffer.Length == 11 && buffer[0] == 'U' && buffer[5] == 0)
                    {
                        string message = "Mavlink 0.9 Heartbeat, Please upgrade your AP, This planner is for Mavlink 1.0\n\n";
                        throw new Exception(message);
                    }
                    return new byte[0];
                }
            }

            if (buffer.Length < 5 || buffer[buffer.Length - 1] != (crc >> 8) || buffer[buffer.Length - 2] != (crc & 0xff))
            {
                int packetno = -1;
                if (buffer.Length > 5)
                {
                    packetno = buffer[5];
                }
                if (packetno != -1 && buffer.Length > 5 && MAVLink.MAVLINK_MESSAGE_INFO[packetno] != null)
                    log.InfoFormat("Mavlink Bad Packet (crc fail) len {0} crc {1} vs {4} pkno {2} {3}", buffer.Length, crc, packetno, MAVLink.MAVLINK_MESSAGE_INFO[packetno].ToString(), BitConverter.ToUInt16(buffer, buffer.Length - 2));
                if (logreadmode)
                    log.InfoFormat("bad packet pos {0} ", logplaybackfile.BaseStream.Position);
                return new byte[0];
            }

            try
            {
                if ((buffer[0] == 'U' || buffer[0] == 254) && buffer.Length >= buffer[1])
                {
                    if (buffer[3] == '3' && buffer[4] == 'D')
                    {

                    }
                    else
                    {


                        byte packetSeqNo = buffer[2];
                        int expectedPacketSeqNo = ((MAV.recvpacketcount + 1) % 0x100);

                        {
                            if (packetSeqNo != expectedPacketSeqNo)
                            {
                                synclost++; // actualy sync loss's
                                int numLost = 0;

                                if (packetSeqNo < ((MAV.recvpacketcount + 1))) // recvpacketcount = 255 then   10 < 256 = true if was % 0x100 this would fail
                                {
                                    numLost = 0x100 - expectedPacketSeqNo + packetSeqNo;
                                }
                                else
                                {
                                    numLost = packetSeqNo - MAV.recvpacketcount;
                                }
                                packetslost += numLost;
                                WhenPacketLost.OnNext(numLost);

                                log.InfoFormat("lost pkts new seqno {0} pkts lost {1}", packetSeqNo, numLost);
                            }

                            packetsnotlost++;

                            MAV.recvpacketcount = packetSeqNo;
                        }
                        WhenPacketReceived.OnNext(1);
                        // Console.WriteLine(DateTime.Now.Millisecond);
                    }

                    //                    Console.Write(temp[5] + " " + DateTime.Now.Millisecond + " " + packetspersecond[temp[5]] + " " + (DateTime.Now - packetspersecondbuild[temp[5]]).TotalMilliseconds + "     \n");

                    if (double.IsInfinity(packetspersecond[buffer[5]]))
                        packetspersecond[buffer[5]] = 0;

                    packetspersecond[buffer[5]] = (((1000 / ((DateTime.Now - packetspersecondbuild[buffer[5]]).TotalMilliseconds) + packetspersecond[buffer[5]]) / 2));

                    packetspersecondbuild[buffer[5]] = DateTime.Now;

                    //Console.WriteLine("Packet {0}",temp[5]);
                    // store packet history
                    lock (objlock)
                    {
                        MAV.packets[buffer[5]] = buffer;
                        MAV.packetseencount[buffer[5]]++;
                    }

                    PacketReceived(buffer);

                    if (buffer[5] == (byte)MAVLink.MAVLINK_MSG_ID.STATUSTEXT) // status text
                    {
                        var msg = MAV.packets[(byte)MAVLink.MAVLINK_MSG_ID.STATUSTEXT].ByteArrayToStructure<MAVLink.mavlink_statustext_t>(6);

                        byte sev = msg.severity;

                        string logdata = Encoding.ASCII.GetString(msg.text);
                        int ind = logdata.IndexOf('\0');
                        if (ind != -1)
                            logdata = logdata.Substring(0, ind);
                        log.Info(DateTime.Now + " " + logdata);

                        MAV.cs.messages.Add(logdata);

                        if (sev >= 3)
                        {
                            MAV.cs.messageHigh = logdata;
                            MAV.cs.messageHighTime = DateTime.Now;
                        }
                    }

                    // set ap type
                    if (buffer[5] == (byte)MAVLink.MAVLINK_MSG_ID.HEARTBEAT)
                    {
                        MAVLink.mavlink_heartbeat_t hb = buffer.ByteArrayToStructure<MAVLink.mavlink_heartbeat_t>(6);

                        if (hb.type != (byte)MAVLink.MAV_TYPE.GCS)
                        {
                            mavlinkversion = hb.mavlink_version;
                            MAV.aptype = (MAVLink.MAV_TYPE)hb.type;
                            MAV.apname = (MAVLink.MAV_AUTOPILOT)hb.autopilot;
                            setAPType();
                        }
                    }

                    getWPsfromstream(ref buffer);

                }
            }
            catch { }

            if (buffer[3] == '3' && buffer[4] == 'D')
            {
                // dont update last packet time for 3dr radio packets
            }
            else
            {
                lastvalidpacket = DateTime.Now;
            }

           

            return buffer;
        }


        List<KeyValuePair<MAVLink.MAVLINK_MSG_ID, Func<byte[], bool>>> Subscriptions = new List<KeyValuePair<MAVLink.MAVLINK_MSG_ID, Func<byte[], bool>>>();


        private void PacketReceived(byte[] buffer)
        {
            MAVLink.MAVLINK_MSG_ID type = (MAVLink.MAVLINK_MSG_ID)buffer[5];

            lock (Subscriptions)
            {
                foreach (var item in Subscriptions)
                {
                    if (item.Key == type)
                    {
                        try
                        {
                            item.Value(buffer);
                        }
                        catch (Exception ex)
                        {
                            log.Error(ex);
                        }
                    }
                }
            }
        }

        private byte[] readlogPacket()
        {
            byte[] temp = new byte[300];

            //MAV.sysid = 0;

            int a = 0;
            while (a < temp.Length && logplaybackfile.BaseStream.Position != logplaybackfile.BaseStream.Length)
            {
                temp[a] = (byte)logplaybackfile.BaseStream.ReadByte();
                //Console.Write((char)temp[a]);
                if (temp[a] == ':')
                {
                    break;
                }
                a++;
                if (temp[0] != '-')
                {
                    a = 0;
                }
            }

            //Console.Write('\n');

            //Encoding.ASCII.GetString(temp, 0, a);
            string datestring = Encoding.ASCII.GetString(temp, 0, a);
            //Console.WriteLine(datestring);
            long date = Int64.Parse(datestring);
            DateTime date1 = DateTime.FromBinary(date);

            lastlogread = date1;

            int length = 5;
            a = 0;
            while (a < length)
            {
                temp[a] = (byte)logplaybackfile.BaseStream.ReadByte();
                if (a == 1)
                {
                    length = temp[1] + 6 + 2 + 1;
                }
                a++;
            }

            return temp;
        }

        private byte[] readlogPacketMavlink(bool smartSearch)
        {
            byte[] temp = new byte[300];

            //MAV.sysid = 0;

            //byte[] datearray = BitConverter.GetBytes((ulong)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds);

            byte[] datearray = new byte[8];

            int tem = logplaybackfile.BaseStream.Read(datearray, 0, datearray.Length);

            Array.Reverse(datearray);

            DateTime date1 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            UInt64 dateint = BitConverter.ToUInt64(datearray, 0);

            try
            {
                // array is reversed above
                if (datearray[7] == 254)
                {
                    //rewind 8bytes
                    logplaybackfile.BaseStream.Seek(-8, SeekOrigin.Current);
                }
                else
                {
                    date1 = date1.AddMilliseconds(dateint / 1000);

                    lastlogread = date1.ToLocalTime();
                }
            }
            catch 
            {
                if (smartSearch)
                    return null;
            }

            //MAV.cs.datetime = lastlogread;

            int length = 5;
            int a = 0;
            while (a < length)
            {
                if (logplaybackfile.BaseStream.Position == logplaybackfile.BaseStream.Length)
                    break;
                temp[a] = (byte)logplaybackfile.ReadByte();
                if (temp[0] != 'U' && temp[0] != 254)
                {
                    log.InfoFormat("logread - lost sync byte {0} pos {1}", temp[0], logplaybackfile.BaseStream.Position);
                    a = 0;
                    continue;
                }
                if (a == 1)
                {
                    length = temp[1] + 6 + 2; // 6 header + 2 checksum
                }
                a++;
            }

            // set ap type for log file playback
            if (temp[5] == 0 && a > 5)
            {
                MAVLink.mavlink_heartbeat_t hb = temp.ByteArrayToStructure<MAVLink.mavlink_heartbeat_t>(6);
                if (hb.type != (byte)MAVLink.MAV_TYPE.GCS)
                {
                    mavlinkversion = hb.mavlink_version;
                    aptype = (MAVLink.MAV_TYPE)hb.type;
                    apname = (MAVLink.MAV_AUTOPILOT)hb.autopilot;
                    setAPType();
                }
            }

            return temp;
        }

        private static byte mavlinkversion;
        private static MAVLink.MAV_TYPE aptype;
        private static MAVLink.MAV_AUTOPILOT apname;

        public static void setAPType()
        {
            //switch (apname)
            //{
            //    case MAV_AUTOPILOT.ARDUPILOTMEGA:
            //        switch (aptype)
            //        {
            //            case MAVLink.MAV_TYPE.FIXED_WING:
            //                MAV.cs.firmware = MainV2.Firmwares.ArduPlane;
            //                break;
            //            case MAVLink.MAV_TYPE.QUADROTOR:
            //                MAV.cs.firmware = MainV2.Firmwares.ArduCopter2;
            //                break;
            //            case MAVLink.MAV_TYPE.TRICOPTER:
            //                MAV.cs.firmware = MainV2.Firmwares.ArduCopter2;
            //                break;
            //            case MAVLink.MAV_TYPE.HEXAROTOR:
            //                MAV.cs.firmware = MainV2.Firmwares.ArduCopter2;
            //                break;
            //            case MAVLink.MAV_TYPE.OCTOROTOR:
            //                MAV.cs.firmware = MainV2.Firmwares.ArduCopter2;
            //                break;
            //            case MAVLink.MAV_TYPE.HELICOPTER:
            //                MAV.cs.firmware = MainV2.Firmwares.ArduCopter2;
            //                break;
            //            case MAVLink.MAV_TYPE.GROUND_ROVER:
            //                MAV.cs.firmware = MainV2.Firmwares.ArduRover;
            //                break;
            //            default:
            //                break;
            //        }
            //        break;
            //    case MAV_AUTOPILOT.UDB:
            //        switch (MAV.aptype)
            //        {
            //            case MAVLink.MAV_TYPE.FIXED_WING:
            //                MAV.cs.firmware = MainV2.Firmwares.ArduPlane;
            //                break;
            //        }
            //        break;
            //    case MAV_AUTOPILOT.GENERIC:
            //        switch (MAV.aptype)
            //        {
            //            case MAVLink.MAV_TYPE.FIXED_WING:
            //                MAV.cs.firmware = MainV2.Firmwares.Ateryx;
            //                break;
            //        }
            //        break;
            //}
        }






        /// <summary>
        /// used as a snapshot of what is loaded on the ap atm. - derived from the stream
        /// </summary>
        public static Dictionary<int, MAVLink.mavlink_mission_item_t> wps = new Dictionary<int, MAVLink.mavlink_mission_item_t>();


        /// <summary>
        /// Store the guided mode wp location
        /// </summary>
        public static MAVLink.mavlink_mission_item_t GuidedMode = new MAVLink.mavlink_mission_item_t();

        public static Dictionary<int, MAVLink.mavlink_rally_point_t> rallypoints = new Dictionary<int, MAVLink.mavlink_rally_point_t>();

        public static Dictionary<int, MAVLink.mavlink_fence_point_t> fencepoints = new Dictionary<int, MAVLink.mavlink_fence_point_t>();

        /// <summary>
        /// Used to extract mission from log file - both sent or received
        /// </summary>
        /// <param name="buffer">packet</param>
        static void getWPsfromstream(ref byte[] buffer)
        {
            if (buffer[5] == (byte)MAVLink.MAVLINK_MSG_ID.MISSION_COUNT)
            {
                // clear old
                wps.Clear();
            }

            if (buffer[5] == (byte)MAVLink.MAVLINK_MSG_ID.MISSION_ITEM)
            {
                MAVLink.mavlink_mission_item_t wp = buffer.ByteArrayToStructure<MAVLink.mavlink_mission_item_t>(6);

                if (wp.current == 2)
                {
                    // guide mode wp
                    GuidedMode = wp;
                }
                else
                {
                    wps[wp.seq] = wp;
                }

                log.Debug(String.Format("WP # {7} cmd {8} p1 {0} p2 {1} p3 {2} p4 {3} x {4} y {5} z {6}", wp.param1, wp.param2, wp.param3, wp.param4, wp.x, wp.y, wp.z, wp.seq, wp.command));
            }

            if (buffer[5] == (byte)MAVLink.MAVLINK_MSG_ID.RALLY_POINT)
            {
                MAVLink.mavlink_rally_point_t rallypt = buffer.ByteArrayToStructure<MAVLink.mavlink_rally_point_t>(6);

                rallypoints[rallypt.idx] = rallypt;

                log.Debug(String.Format("RP # {0} {1} {2} {3} {4}", rallypt.idx, rallypt.lat, rallypt.lng, rallypt.alt, rallypt.break_alt));
            }

            if (buffer[5] == (byte)MAVLink.MAVLINK_MSG_ID.FENCE_POINT)
            {
                MAVLink.mavlink_fence_point_t fencept = buffer.ByteArrayToStructure<MAVLink.mavlink_fence_point_t>(6);

                fencepoints[fencept.idx] = fencept;
            }
        }



        public void Dispose()
        {
            if (_bytesReceivedSubj != null)
                _bytesReceivedSubj.Dispose();
            //if (_bytesSentSubj != null)
            //    _bytesSentSubj.Dispose();
            this.Close();
        }

        public void Close()
        {
            //try
            //{
            //    if (logfile != null)
            //        logfile.Close();
            //}
            //catch { }
            try
            {
                if (rawlogfile != null)
                    rawlogfile.Close();
            }
            catch { }
            try
            {
                if (logplaybackfile != null)
                    logplaybackfile.Close();
            }
            catch { }

            //try
            //{
            //    if (BaseStream.IsOpen)
            //        BaseStream.Close();
            //}
            //catch { }
        }
    }
}

﻿using System;
using MissionPlannerUtilities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MissionPlannerGeotagger
{
    class Program
    {
        static void Main(string[] args)
        {
            var result = MainAsync(args).GetAwaiter().GetResult();
            Environment.Exit(result);
        }
        static async Task<int> MainAsync(string[] args)
        {
            if (args.Length <= 0)
            {
                Console.WriteLine("No arguments provided.");
                return 1;
            }
            try
            {
                Console.WriteLine("Args: " + String.Join(" | ", args));
                if (args[0] == "get-end")
                {
                    var endOfFlight = MAVLinkUtil.GetEndTimeOfFlight(args[1]);
                    Console.WriteLine("End of Flight:" + endOfFlight.Result.ToString());
                }
                else if (args[0] == "geo-ref")
                {
                    if (args.Length < 4)
                        throw new ArgumentException("Usage: geo-ref input-image-directory telemetry-log-file offset");

                    string inputDir = args[1];
                    if (!Directory.Exists(inputDir))
                        throw new ArgumentException(inputDir + " does not exist");

                    string telemetryLogFile = args[2];
                    if (!File.Exists(telemetryLogFile))
                        throw new ArgumentException(inputDir + " is not a valid telemetry log or does not exist");

                    float offset;
                    if (!float.TryParse(args[3], out offset))
                        throw new ArgumentException(args[3] + " could not be parsed as an offset");

                    bool useGpsTime = args.Contains("-gps");
                    bool overwrite = args.Contains("-overwrite");
                    bool printTaggedInformation = args.Contains("-print");

                    Console.WriteLine("Running with offset of " + offset + ", gps camera: " + useGpsTime);

                    GeoReferencer geoRef = new GeoReferencer(directoryWithImages: inputDir,
                        telemetryLogFilePath: telemetryLogFile, useGPSTime: useGpsTime,
                        overWriteOriginalImages: overwrite);

                    var taggedImages = await geoRef.GeoTagPhotosAsync(offset, true);

                    int taggedImageCount = 0;
                    if (taggedImages != null)
                        // Subtract the header
                        taggedImageCount = taggedImages.Length - 1;

                    Console.WriteLine("Total Tagged Images: " + taggedImageCount);
                    
                    if (printTaggedInformation)
                    {
                        Console.WriteLine("Printing Tagged Images");
                        for(int i = 0; i < taggedImages.Length; ++i)
                        {
                            if (i == 0)
                                Console.WriteLine("Output Format:" + taggedImages[i]);
                            else
                                Console.WriteLine("Tagged Image:" + taggedImages[i]);
                        }
                    }
                }
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
                return 1;
            }

            Console.WriteLine("Finished");
            return 0;
        }
    }
}

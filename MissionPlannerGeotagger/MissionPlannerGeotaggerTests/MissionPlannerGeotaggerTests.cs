﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MissionPlannerUtilities;
using System.IO;

namespace MissionPlannerGeotaggerTests
{
    [TestClass]
    public class TestGeoreferencer
    {
        private static string GPS_IMAGES_DIR = Path.Combine("TestFiles", "GPSImages");

        [TestMethod]
        public void TestGeoreferencerWithGPSCamera()
        {
            GeoReferencer geoRef = new GeoReferencer(GPS_IMAGES_DIR,
                Path.Combine(GPS_IMAGES_DIR, "2014-05-29 10-46-59.tlog"), true,
                false);

            geoRef.GeoTagPhotos(0, false);

            Assert.AreEqual(1, 0);
        }
        
        [TestMethod]
        public void TestGetEndOfFlight()
        {
            var endOfFlight = MAVLinkUtil.GetEndTimeOfFlight(Path.Combine("TestFiles", "2018-03-21 09-02-39.tlog"));
            Assert.AreEqual("2018-03-21 9:11:41 AM", endOfFlight.Result.ToString());
        }

        [TestMethod]
        public void TestConvertLogFileNameToValidDAteTime()
        {
            var startOfFlight = MAVLinkUtil.ConvertLogFileNameToValidDateTime(@"C:\Users\Paul\Documents\Mission Planner\logs\QUADROTOR\1\2018-03-21 11-12-38.tlog");
            Assert.AreEqual("2018-03-21 11:12:38 AM", startOfFlight.ToString());

            startOfFlight = MAVLinkUtil.ConvertLogFileNameToValidDateTime(@"2018-03-21 11-12-38.tlog");
            Assert.AreEqual("2018-03-21 11:12:38 AM", startOfFlight.ToString());
        }
    }
}

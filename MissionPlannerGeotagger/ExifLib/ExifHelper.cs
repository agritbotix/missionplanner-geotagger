﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExifLib
{
    public static class ExifHelper
    {
        /// <summary>
        /// Renders a tag into a string value
        /// </summary>
        /// <param name="tagValue"></param>
        /// <returns></returns>
        public static string RenderTag(object tagValue)
        {
            // Special case - some doubles are encoded as TIFF rationals. These
            // items can be retrieved as 2 element arrays of {numerator, denominator}
            //if (tagValue is double)
            //{
            //    int[] rational = tagValue as int [];
            //    tagValue = string.Format("{0} ({1}/{2})", tagValue, rational[0], rational[1]);
            //}

	        // Arrays don't render well without assistance.
	        var array = tagValue as Array;
	        if (array != null)
	        {
                string output = string.Empty;
                // Hex rendering for really big byte arrays (ugly otherwise)
                if (array.Length > 20 && array.GetType().GetElementType() == typeof(byte))
                {
                    for (int i = 0; i < array.Length; ++i)
                        output += ((byte)array.GetValue(i)).ToString("X2");
                    return "0x" + output;
                }
                for (int i = 0; i < array.Length; ++i)
                    output += ((object)array.GetValue(i)).ToString() + ", ";
                output.TrimEnd(',', ' ');
                return output;
	        }

	        return tagValue.ToString();
        }
    }
}
